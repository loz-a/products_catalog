export default {
  __required: {
    title: true
  },

  title: (value, context, dependencies) => {
    const errors = []
    if (typeof value !== 'string') errors.push('Значення введене не вірно')
    if (!value.trim().length) errors.push('Значення не може бути пустим')

    const MAX_LENGTH = dependencies.config.validate().catalog.title.maxLength
    if (value.length > MAX_LENGTH) errors.push(`Довжина значення не повинна перевищувати ${MAX_LENGTH} символів. Поточна довжина ${value.length}`)

    if (!context.hasOwnProperty('id')) {
      const isCatalogExists = !!Object.values(dependencies.store.catalogs.all()).filter((item) => item.title == value.trim()).length
      if (isCatalogExists) errors.push('Запис з таким значенням вже існує')
    }

    return (errors.length) ? errors : true
  }
}