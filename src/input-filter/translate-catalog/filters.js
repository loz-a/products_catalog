export default {
  title: (value) => {
    if (typeof value !== 'string') return value
    return value.trim()
  }
}