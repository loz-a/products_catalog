import InputFilter from 'app/utils/input-filter'
import validators from './validators'
import filters from './filters'

export default new InputFilter(validators, filters)