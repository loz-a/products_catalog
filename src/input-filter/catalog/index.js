import InputFilter from 'app/utils/input-filter'
import validators from './validators'
import filters from './filters'

// const postProcessor = (catalog) => {  
//   const data = {
//     title: catalog.title,
//     alias: catalog.alias,
//     icon_src: catalog.icon
//   }

//   return data
// }

// export default new InputFilter(validators, filters, postProcessor)
export default new InputFilter(validators, filters)