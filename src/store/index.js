import { extendObservable, observable, action, toJS } from 'mobx'
import schema from './schema'
import shortid from 'shortid'

import transformCatalogs from './transformers/catalogs'
import transformCatalogsAsTree from './transformers/catalogs-as-tree'

export default function(INIT_STATE = {}, config) {
  const initState = schema.normalizeInitState(INIT_STATE, config)

  const dataSource = {}  

  extendObservable(dataSource, {    
    catalogs: observable.map(initState.catalogs),
    products: observable.map({}), 
    relatedProducts: observable.map({}),   
    flashMessages: observable.shallowMap({})
  })

  this.dataSource = dataSource

  this.catalogs = {
    _entityRequiredFieldsKeys: ['id', 'parentId', 'title', 'alias'],

    all: () => transformCatalogs(this),

    asTree: () => transformCatalogsAsTree(this),

    byId: (id) => {
      if (!dataSource.catalogs.has(id)) throw new Error(`Catalog with id: ${id} is not exists in store`)
      return dataSource.catalogs.get(id)
    },    

    save: action((denormalizedData) => {    
      const normalizedData = schema.normalizeCatalogs(denormalizedData, config)
      const data = normalizedData.entities.catalogs[normalizedData.result]

      if (dataSource.catalogs.has(data.id)) {
        const entity = dataSource.catalogs.get(data.id)
        dataSource.catalogs.set(data.id, {...entity, ...data})
        return this.catalogs
      }

      this.catalogs._entityRequiredFieldsKeys.forEach((key) => {
        if (typeof data[key] === 'undefined') throw new Error(`data.${key} is required and should not be empty`)
      })
      dataSource.catalogs.set(data.id, data)
      return this.catalogs
    }), 

     delete: action((catalog) => {            
      dataSource.catalogs.delete(catalog.id)
    }),    

    size: () => dataSource.catalog.size
  }

  this.products = {  

    populate: action((data) => {
      if (!Array.isArray(data)) throw new Error('Expected variable of an Array type ')      
      dataSource.products.merge(schema.normalizeProducts(data).entities.products)
    }),

    byId: (id) => {       
      if (!dataSource.products.has(id)) {        
        throw new Error(`Product with id: ${id} is not exists in store`)
      }
      const product = toJS(dataSource.products.get(id))      
      return {
        ...product,
        catalog: this.catalogs.byId(product.catalogId),
        related: this.products.related.byOwner(product)
      }
    },

    has:(id) => dataSource.products.has(id),

    save: action((data) => {   
      dataSource.products.set(data.id, schema.normalizeProduct(data))
    }),

    delete: action((product) => {
      dataSource.products.delete(product.id)
    }),

    size: () => dataSource.products.size,

    related: {
      byOwner: (owner) => {
        const relProducts = dataSource.relatedProducts
        const ownerId = owner.id
        
        if (relProducts.has(ownerId)) {
          const relProductsData = relProducts.get(ownerId)
          
          return Object
            .keys(relProductsData)            
            .reduce((acc, productId) => {
              acc.push({
                ...(toJS(this.products.byId(productId))),
                priority: relProductsData[productId]
              })
              return acc
            }, [])
            .sort((prod1, prod2) => {
              return prod1.priority - prod2.priority
            })
            .reverse()
        }

        return []
      },

      save: action((owner, data) => {
        const relProducts = dataSource.relatedProducts
        const { id, priority } = data
   
        const ownerData = relProducts.has(owner.id) 
          ? { ...relProducts.get(owner.id), [id]: priority }
          : { [id]: priority }
   
        relProducts.set(owner.id, ownerData)        
      }),

      batchSave: action((owner, data) => {        
        const relProds = dataSource.relatedProducts
        const ownerId = owner.id
                
        if (relProds.has(ownerId)) {
          relProds.delete(ownerId)
        }

        data.forEach((item) => this.products.related.save(owner, item))
      })
    }
  }

  this.flashMessages = {    
    messages: () => {
      const fm = dataSource.flashMessages
      return fm.keys().map((id) => fm.get(id))
    },
    clear: () => dataSource.flashMessages.clear(),
    add: (type, text, header = null) => {
      const id = shortid.generate()
      dataSource.flashMessages.set(id, {id, type, text, header })
    },
    delete: (id) => {
      if (dataSource.flashMessages.has(id)) dataSource.flashMessages.delete(id)
    },
    warn: (msg, header = null) => this.flashMessages.add('warning', msg, header),
    error: (msg, header = null) => this.flashMessages.add('error', msg, header),
    success: (msg, header = null) => this.flashMessages.add('success', msg, header),
    info: (msg, header = null) => this.flashMessages.add('info', msg, header)
  }
}
