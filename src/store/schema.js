import { schema, normalize } from 'normalizr'

const getCatalogEntitySchema = (config) => {
  const lang = config.languages.current()  
  return new schema.Entity('catalogs', {}, {
    processStrategy: (entity, parent) => ({
        id: entity.id,
        alias: entity.alias,
        title: typeof entity.translations[lang] !== 'undefined' ? entity.translations[lang].title : '',
        icon: entity.icon_src,
        parentId: typeof entity.parent_id !== 'undefined' ? entity.parent_id : null,
        translations: entity.translations
      })
  })
}

const productSchema = new schema.Entity('products', {}, {
  processStrategy: (entity) => ({
    id: entity.id,
    catalogId: entity.catalog_type,
    title: entity.name,
    price: entity.palett_height,
    priceUAH: entity.price_uah,
    imgSrc: entity.img_src,
    imgSrcBig: entity.img_src_big,
    color: entity.color,
    diameter: entity.diameter,
    height: entity.height,
    manhole: entity.manhole,
    material: entity.material,
    orderId: entity.order_id,
    palett: entity.palett,
    volume: entity.volume,
    weight: entity.weight
  })
})

const productsSchema = new schema.Array(productSchema)

export default {
  normalizeInitState: (INIT_STATE, config) => {    
    return normalize(
      INIT_STATE,
      {
        catalogs: new schema.Array(getCatalogEntitySchema(config))  
      }
    ).entities
  },

  normalizeCatalogs: (data, config) => normalize(data, getCatalogEntitySchema(config)),

  normalizeProducts: (data) => normalize(data, productsSchema),

  normalizeProduct: (data) => {
    const normalizedData = normalize(data, productSchema)
    return normalizedData.entities.products[normalizedData.result]
  }
}