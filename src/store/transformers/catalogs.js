import { createTransformer, toJS } from 'mobx'

export default createTransformer((store) => {  
  const catalogs = store.dataSource.catalogs
  return catalogs.keys().reduce((acc, id) => {
    acc[id] = catalogs.get(id)
    return acc
  }, {})
})
