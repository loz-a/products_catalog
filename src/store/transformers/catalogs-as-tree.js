import { createTransformer, toJS } from 'mobx'

function transform(values, parent, allCatalogs, allProducts) {
  return values.reduce((acc, value) => {    
    const { id, title } = value
            
    const catalog = { parent, id, title }

    if (typeof value['parentId'] !== 'undefined') {
      const childrenCatalogs = allCatalogs.filter((item) => id === item['parentId'])
      const childrenProducts = allProducts.filter((item) => id === item['catalogId']).sort((item1, item2) => item1.orderId - item2.orderId)

      const children = [
        ...childrenCatalogs,
        ...childrenProducts
      ]
      catalog.children = transform(children, catalog, allCatalogs, allProducts)
    }

    acc.push(catalog)

    return acc
  }, [])
}


export default createTransformer((store) => {
  const catalogs = Object.values(toJS(store.dataSource.catalogs))
  const products = Object.values(toJS(store.dataSource.products))

  return transform(
    catalogs.filter((item) => null === item.parentId), 
    null, 
    catalogs,
    products
  )
})