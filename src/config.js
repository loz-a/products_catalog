export default function(CONFIG = {}) {
    
  const { urls, languages, validate } = CONFIG
  let availableLanguages

  const isDefined = (key) => {
    if (typeof languages[key] === 'undefined') {
        throw new Error(`languages.${key} is undefined`)
      }
  }

  const cacheLanguages = () => {
    isDefined('availableLanguages')
    availableLanguages = languages.availableLanguages.reduce((acc, item) => {
      acc[item.language] = item
      return acc
    }, {})
  }

  this.languages = {

    current: () => {
      isDefined('currentLanguage')
      return languages.currentLanguage
    },

    byId: (id) => {
      const languages = this.languages.availables()
      if (!languages[id]) throw new Error(`Language with ${id} id is undefined`)
      return languages[id]
    },

    availables: () => {           
      if (!availableLanguages) cacheLanguages()
      return availableLanguages
    }
  }

  this.urlFor = (alias) => {
    if (!urls || !urls[alias]) throw new Error('Url with this alias is undefined')
    return urls[alias]
  }

  this.validate = () => validate
  
}