import React from 'react'
import { render } from 'react-dom'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { useStrict } from 'mobx'
import { Provider } from 'mobx-react'
import Config from './config'
import Store from './store'
import Api from './api'
import App from './components/app/index'
import BreadcrumbsProvider from './components/common/breadcrumbs/provider'

import DevTools from 'mobx-react-devtools'

useStrict(true)

$(document).ready(() => {

  const config = new Config(window.INIT_STATE.config)
  const store = new Store(window.INIT_STATE, config)
  const api = new Api(config)

  if (DEVELOPMENT) {
    window.store = store
    window.config = config
  }

  render(
    <div>
      <Provider config={config} store={store} api={api}>
        <Router>
          <BreadcrumbsProvider>
            <Route path="/uk/editor/catalogs" component={App} />
          </BreadcrumbsProvider>
        </Router>
      </Provider>
      {DEVELOPMENT && <DevTools/>}
    </div>,
    document.getElementById('products_catalog')
  )

})



// function Folder(parent, name) {
//   this.parent = parent;
//   this.name = "" + name;
//   this.children = [];
// }

// function DisplayFolder(folder, state) {
//   this.state = state;
//   this.folder = folder;
//   this.collapsed = false;
//   this._children = folder.children.map(transformFolder);
// }

// Object.defineProperties(DisplayFolder.prototype, {
//   name: {
//     get: function () {
//       return this.folder.name;
//     }
//   },
//   isVisible: {
//     get: function () {
//       return !this.state.filter || this.name.indexOf(this.state.filter) !== -1 || this.children.some(child => child.isVisible);
//     }
//   },
//   children: {
//     get: function () {
//       if (this.collapsed)
//         return [];
//       return this._children.filter(function (child) {
//         return child.isVisible;
//       });
//     }
//   },
//   path: {
//     get: function () {
//       return this.folder.parent === null ? this.name : transformFolder(this.folder.parent).path + "/" + this.name;
//     }
//   }
// });

// var state = {
// 		root: new Folder(null, "root"),
// 		filter: null,
// 		displayRoot: null
// 	};
	
// 	var transformFolder = function (folder) {
// 		return new DisplayFolder(folder, state);
// 	};

// // returns list of strings per folder
// 	DisplayFolder.prototype.rebuild = function () {
// 		var path = this.path;
// 		this.asText = path + "\n" +
// 			this.children.filter(function(child) {
// 				return child.isVisible;
// 			}).map(child => child.asText).join('');
// 	};
	
// 	DisplayFolder.prototype.rebuildAll = function() {
// 		this.children.forEach(child => child.rebuildAll());
// 		this.rebuild();
// 	}

//   function createFolders(parent, recursion) {
// 		if (recursion === 0) {
// 			return;
//     }
// 		for (var i = 0; i < 10; i++) {      
// 			var folder = new Folder(parent, i);
// 			parent.children.push(folder);
// 			createFolders(folder, recursion - 1);
// 		}
// 	}

  
//   createFolders(state.root, 3)
  
//   state.displayRoot = transformFolder(state.root)
//   state.displayRoot.rebuildAll();
//   state.text = state.displayRoot.asText.split("\n");

//   console.dir(state)
