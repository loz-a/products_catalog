import React from 'react'
import { Route, withRouter } from 'react-router-dom'
import { observer } from 'mobx-react'
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import FlashMessages from '../common/flash-messages/index'
import breadcrumbs from 'app/components/common/breadcrumbs'
import Tree from './tree'
import Content from './content'
import './styles.css'

const App = ({ 
  renderBreadcrumbs,
  match
}) => {    
  return (
    <div className="app-container">
      <FlashMessages />
      {renderBreadcrumbs()}

      <div className="row-fluid">
        <div className="span3">
          <Route path={`${match.path}/:catalogId(\\d+)?/(.*)?`} component={Tree}/>          
        </div>
        <div className="span9">
          <Content/>
        </div>          
      </div>

    </div>
  )
}

export default DragDropContext(HTML5Backend)(
  withRouter(
    breadcrumbs('Каталог товарів')(
      observer(App)
    )
  )
)