import React from 'react'
import { Route, withRouter, Switch } from 'react-router-dom'
import Wellcome from './wellcome'
import NewCatalog from './catalog/new'
import ViewCatalog from './catalog'
import PageNotFound from 'app/components/common/page-not-found'


const Content = ({
  match
}) => {  

  return (
    <div className="catalog-data-manipulation-box">
      <Switch>
        <Route exact path={match.path} component={Wellcome}/>
        <Route path={`${match.path}/add-catalog`} component={NewCatalog}/>
        <Route path={`${match.path}/:catalogId(\\d+)`} component={ViewCatalog}/>
        <Route render={() => (<PageNotFound/>)} />
      </Switch>
    </div>
  )
}


export default withRouter(Content)