import React, { PropTypes } from 'react'
import { withRouter, Link } from 'react-router-dom'

const CatalogActions = ({ match }) => (
  <ul className="nav nav-pills">
    <li>
      <Link to={`${match.url}/new-product`}>
        <i className="zmdi zmdi-plus"></i> Додати товар
      </Link>
    </li>           
    <li>
      <Link to={`${match.url}/edit`}>
        <i className="zmdi zmdi-edit"></i> Редагувати
      </Link>
    </li>
    <li>
      <Link to={`${match.url}/delete`}>
        <i className="zmdi zmdi-delete"></i> Видалити
      </Link>
    </li>
  </ul>
)

CatalogActions.propTypes = {
  match: PropTypes.object.isRequired
}

export default withRouter(CatalogActions)