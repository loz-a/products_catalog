import React from 'react'
import { withRouter } from 'react-router-dom'
import { computed, observable, action } from 'mobx'
import { inject, observer } from 'mobx-react'
import breadcrumbs from 'app/components/common/breadcrumbs'
import Loader from 'app/components/common/loader'
import Submit from 'app/components/common/form/link/submit'
import RelatedList from './related-list'
import './styles.css'

@breadcrumbs('Асоційовані товари')
@withRouter
@inject('store', 'api')
@observer
class RelatedProducts extends React.PureComponent {

  @observable items = observable.array([])
  @observable isLoading = false
    
  componentWillMount() {    
    this.populateItems()  
  } 
  
  @computed get productsStore() {
    return this.props.store.products
  }

  @computed get owner() {
    const { store, match } = this.props        
    return store.products.byId(match.params.productId)
  }

  @computed get flashMessages() {
    return this.props.store.flashMessages
  }

  @computed get productUrl() {
    return this.props.match.url.split('/').slice(0, -1).join('/')
  }

  render() {
    return (
      <Loader show={this.isLoading} message={'Перекуріть. Дані обробляються...'}>
        {this.renderList()}
      </Loader>
    )
  }

  renderList() {    
    return (
      <div className="related-products">
        <div className="alert">  
          <strong><i className="zmdi zmdi-hc-lg zmdi-info-outline"></i> Увага! </strong> 
          Для того щоб додати до списку асоційованих товарів, перетягніть елемент з дерева каталогу товарів
        </div>

        <div className="actions">
          <Submit className="sbmt-btn" onClick={this.handleSubmit} href="/" value="Зберегти" />
        </div>

        <RelatedList 
          relatedProducts={this.items}
          addRelatedProductById={this.addById}
          deleteRelatedProdctById={this.deleteById}/>
      </div>
    )
  }

  @action addById = (id) => {    
    const exists = this.items.filter((item) => item.id == id).length > 0    
    if (exists) {
      this.flashMessages.warn(`Такий асоціативний зв'язок вже присутній у списку`, 'Увага.')    
      return
    }

    const product = this.productsStore.byId(id)
    product.priority = 0
    this.items.push(product)
    this.flashMessages.success(`Новий асоціативний зв'язок успішно створено. Будь ласка, збережіть зміни`, 'Успіх.')    
  }

  @action deleteById = (id) => {
    const product = this.items.find((item) => item.id == id)
    if (!product) throw new Error(`Related product with id ${id} is undefined`)
    this.items.remove(product)
    this.flashMessages.success(`Асоціативний зв'язок видалено. Будь ласка, збережіть зміни`, 'Успіх.')    
  }

  handleSubmit = action((evt) => {
    evt.preventDefault()
    const { api } = this.props
    this.isLoading = true

    const data = this.items.reduce((acc, product) => {
      const { id, priority } = product
      acc.push({ id, priority })
      return acc
    }, [])

    api.product.related
      .save({
        ownerId: this.owner.id, 
        data
      })
      .then(action((result) => {        
        this.isLoading = false
        this.productsStore.related.batchSave(this.owner, data)
        this.flashMessages.success(`Дані успішно збережені`, 'Успіх.')        
        this.props.history.push(this.productUrl)
      }))
      .catch(action((error) => {
        this.isLoading = false        
        this.flashMessages.error('Щось пішло не так. Дані не збережені', 'Помилка.')        
      }))
  })

  @action populateItems = () => {    
    const { api } = this.props    
    this.isLoading = true

    api.product.related
      .byOwnerId(this.owner.id)
      .then(action((data) => {        
        this.productsStore.related.batchSave(this.owner, data)
        data.forEach((item) => this.productsStore.save(item))

        const items = this.owner.related
        
        this.items.replace(items)
        this.isLoading = false
      }))
      .catch(action((error) => {           
        this.isLoading = false        
        this.flashMessages.error('Щось пішло не так. Дані не збережені', 'Помилка.')        
      }))
  }

}

export default RelatedProducts