import React, { PropTypes } from 'react'
import { Route, withRouter } from 'react-router-dom'
import { computed } from 'mobx'
import { observer } from 'mobx-react'
import { DropTarget } from 'react-dnd'
import cx from 'classnames'
import dndItemTypes from 'app/components/app/dnd/item-types'
import RelatedListItem from './item'

const spec = {
  drop(props, monitor) {
    const { owner, store } = props
    const item = monitor.getItem()
    
    props.addRelatedProductById(item.id)
    
    return { name: 'RelatedList' }
  }
}

@DropTarget(
  dndItemTypes.CATALOG_ITEM,
  spec,
  (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop()
  })
)
@withRouter
@observer
class RelatedList extends React.PureComponent {  

  static propTypes = {
    connectDropTarget: PropTypes.func.isRequired,
    isOver: PropTypes.bool.isRequired,
    canDrop: PropTypes.bool.isRequired,
    relatedProducts: PropTypes.object.isRequired,
    addRelatedProductById: PropTypes.func.isRequired,
    deleteRelatedProdctById: PropTypes.func.isRequired
  } 

  @computed get relatedProducts() {
    return this.props.relatedProducts    
  }   

  render() {
    const { connectDropTarget } = this.props            

    return connectDropTarget(
      <div>
         { this.renderList() }
      </div>
    )
  }

  renderList = () => {
    const { canDrop, isOver, deleteRelatedProdctById, match } = this.props    
    const isActive = canDrop && isOver        
    const listCssClass = cx('related-products-list', { 'active': isActive })
    
    return (
      <ul className={listCssClass}>
        {this.relatedProducts.map((product) => (
          <RelatedListItem key={product.id} 
            product={product} deleteRelatedProdctById={deleteRelatedProdctById}/>          
        ))}
      </ul>
    )    
  }
}

export default RelatedList