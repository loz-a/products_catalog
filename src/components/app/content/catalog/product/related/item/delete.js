import React, { PropTypes } from 'react'
import { withRouter } from 'react-router-dom'
import Yes from 'app/components/common/form/link/yes'
import No from 'app/components/common/form/link/no'

@withRouter
class DeleteReletedProduct extends React.PureComponent {

  static propTypes = {
    deleteProduct: PropTypes.func.isRequired
  }

  get relatedListUrl() {
    const { match } = this.props    
    return match.url.split('/').slice(0, -2).join('/')
  }

  render() {    
    return (
      <div className="delete-alert">
        <strong>Увага!</strong> Ви дійсно бажаєте видалити асоціацію з цим продуктом?
        <Yes onClick={this.handleSubmit} title="Так" className="danger-btn" />
        <No href={this.relatedListUrl} title="Ні" />
      </div>
    )
  }

  handleSubmit = (evt) => {    
    evt.preventDefault()
    this.props.deleteProduct()
    this.props.history.replace(this.relatedListUrl)
  }

}

export default DeleteReletedProduct