import React, { PropTypes } from 'react'
import { Link, withRouter, Route } from 'react-router-dom'
import { action } from 'mobx'
import { observer, inject } from 'mobx-react'
import pathToRegexp from 'path-to-regexp'
import Delete from 'app/components/common/form/link/delete'
import DeleteReletedProduct from './delete'

@withRouter
@inject('store')
@observer
class RelatedListItem extends React.PureComponent {

  static propTypes = {
    product: PropTypes.object.isRequired,
    deleteRelatedProdctById: PropTypes.func.isRequired
  }

  render() {    
    const { match, product } = this.props
    const priority = product.priority
    return (
      <li>
        <div className="details">
          <img src={product.imgSrc} alt=""/>

          <ul className="unstyled">
            <li>
              Назва: <Link to={this.productUrl(product.catalog.id, product.id)}>{product.title}</Link>                  
            </li>
            <li>
              Каталог: <Link to={this.catalogUrl(product.catalog.id)}>{product.catalog.title}</Link>
            </li>
          </ul>
        
        <Route
          path={`${match.path}/delete/:relatedProductId(\\d+)`}
          children={({match: routeMatch}) => {
            return routeMatch ? null : (
              <ul className="unstyled">
              <li>
                Ціна: {product.price || product.priceUAH}
              </li>
              <li>                    
                Пріоритет: <input type="text" value={priority} onChange={this.changePriority(product)} />                                                            
              </li>
            </ul>)
          }}/>
          
        </div>        
    
        <Route
          path={`${match.path}/delete/:relatedProductId(\\d+)`}
          children={({match: routeMatch}) => {
            const paramId = routeMatch && routeMatch.params.relatedProductId
            
            if (paramId == product.id) {
              return <DeleteReletedProduct deleteProduct={this.deleteById(product.id)}/>
            }

            return (
              <div className="actions">
                <Delete className="del-btn" 
                    href={`${match.url}/delete/${product.id}`} 
                    value="Видалити" 
                    title="Видалити асоціацію з товаром"/>
              </div>
            )
          }}/>

      </li>
    )
  }

  changePriority = (product) => action((evt) => {
    const priority = evt.target.value
    const isInt = !isNaN(parseInt(priority)) && isFinite(priority)

    if (isInt) {
      product.priority = priority
      return this.props.store.flashMessages.success(`Пріоритет товару змінено. Будь ласка, збережіть зміни`, 'Успіх.')
    }
  })

  deleteById = (id) => () => {    
    this.props.deleteRelatedProdctById(id)
  }  

  productUrl = (catalogId, productId) => {
    const path = this.props.match.path.split('/').slice(0, -1).join('/')
    return pathToRegexp.compile(path)({catalogId, productId}) 
  }
  

  catalogUrl = (catalogId) => {
    const path = this.props.match.path.split('/').slice(0, -3).join('/')
    return pathToRegexp.compile(path)({catalogId}) 
  }

}

export default RelatedListItem