import React, { PropTypes } from 'react'
import cx from 'classnames'

const FormControl = ({
  name,
  label,
  onChange,
  value = '',
  placeholder = '',
  isLoading = flase,
  error = '',
  ...otherProps
}) => {
  const hasError = !!error.length
  return (
    <div className={ cx('control-group', {'error': hasError}) }>
      <label className="control-label" htmlFor={`form-${name}`}>{label}:</label>
      <div className="controls">

        <div>
          <input type="text"
            id={`form-${name}`}
            className="input-large" 
            placeholder={placeholder}
            value={value}
            readOnly={isLoading}
            onChange={onChange}                          
            {...otherProps}/>           
        </div>

        {hasError &&
          <span className="help-inline">
            <i className="zmdi zmdi-alert-triangle"></i> {error}
          </span>
        }
      </div>
    </div>
  )
}

FormControl.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  isLoading: PropTypes.bool,
  error: PropTypes.string
}

export default FormControl