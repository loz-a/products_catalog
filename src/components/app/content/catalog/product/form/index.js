import React, { PropTypes } from 'react'
import { withRouter } from 'react-router-dom'
import { computed, observable, action } from 'mobx'
import { observer, inject } from 'mobx-react'
import ImagePreview from 'app/components/common/image-preview'
import Loader from 'app/components/common/loader'
import FormControl from './control'

@withRouter
@inject('store', 'api', 'config')
@observer
class ProductForm extends React.PureComponent {
  @observable title = ''
  @observable price = ''
  @observable priceUAH = ''
  @observable palett = ''
  @observable color = ''
  @observable diameter = ''
  @observable height = ''
  @observable manhole = ''
  @observable material = ''
  @observable volume = ''
  @observable weight = ''

  @observable imgThumb = observable.shallowObject({ file: null, previewUrl: ''})
  @observable img = observable.shallowObject({ file: null, previewUrl: ''})

  @observable errors = observable.map({})
  @observable isLoading = false

   @observable isImgRemoving = false
   @observable isImgThumbRemoving = false

  constructor(props) {
    super(props)

    if (this.isProductEditing) {
      this.populate(props.product)
    }
  }

  componentWillReceiveProps(nextProps) {
    this.populate(nextProps.product)
  }  
  
  @computed get flashMessages() {
    return this.props.store.flashMessages
  }

  @computed get isProductEditing() {
    return this.props.product.hasOwnProperty('id')
  }

  @computed get product() {
    const product = {
      title: this.title, 
      price: this.price, 
      priceUAH: this.priceUAH, 
      palett: this.palett, 
      color: this.color, 
      diameter: this.diameter, 
      height: this.height, 
      manhole: this.manhole, 
      material: this.material, 
      volume: this.volume, 
      weight: this.weight,
      imgSrc: this.img.file,
      imgSrcBig: this.imgThumb.file
    }

    if (this.isProductEditing) product.id = this.props.product.id
    return product
  }

  render() {    
    return (
      <Loader show={this.isLoading} message={'Перекуріть. Дані обробляються...'}>
        {this.renderForm()}
      </Loader>
    )
  }

  renderForm = () => {  
    return (
      <form 
        autoComplete="off"
        className="form-horizontal catalog-add" 
        onSubmit={this.handleSubmit}>

        <div className="content-header">
            <ul className="nav nav-pills">                     
              <li>
                <button type="submit" className="btn btn-link" disabled={this.isLoading}>
                  <i className="zmdi zmdi-check"></i> {this.isProductEditing ? 'Редагувати' : 'Додати'}
                </button>
              </li>
            </ul>
          </div>
          
          <div className="form-columns-container">
            <div className="column-left">

              <FormControl 
                name="title"
                label="Назва"
                onChange={this.handleChange('title')}
                value={this.title}
                placeholder="Введіть назву продукту"
                isLoading={this.isLoading}
                error={this.errors.has('title') ? this.errors.get('title') : ''}/>

              <FormControl 
                name="price"
                label="Ціна"
                onChange={this.handleChange('price')}
                value={this.price}
                placeholder="Введіть ціну"
                isLoading={this.isLoading}
                error={this.errors.has('price') ? this.errors.get('price') : ''}/>

              <FormControl 
                name="priceUAH"
                label="Ціна в грн."
                onChange={this.handleChange('priceUAH')}
                value={this.priceUAH}
                placeholder="Введіть ціну в грн."
                isLoading={this.isLoading}
                error={this.errors.has('priceUAH') ? this.errors.get('priceUAH') : ''}/>

              <FormControl 
                name="palett"
                label="В упаковці"
                onChange={this.handleChange('palett')}
                value={this.palett}
                placeholder="Введіть кількість в упаковці"
                isLoading={this.isLoading}
                error={this.errors.has('palett') ? this.errors.get('palett') : ''}/>

              <FormControl 
                name="color"
                label="Колір"
                onChange={this.handleChange('color')}
                value={this.color}
                placeholder="Введіть колір"
                isLoading={this.isLoading}
                error={this.errors.has('color') ? this.errors.get('color') : ''}/>

              <FormControl 
                name="diameter"
                label="Діаметер"
                onChange={this.handleChange('diameter')}
                value={this.diameter}
                placeholder="Введіть діаметер"
                isLoading={this.isLoading}
                error={this.errors.has('diameter') ? this.errors.get('diameter') : ''}/>

              <FormControl 
                name="height"
                label="Висота"
                onChange={this.handleChange('height')}
                value={this.height}
                placeholder="Введіть висоту"
                isLoading={this.isLoading}
                error={this.errors.has('height') ? this.errors.get('height') : ''}/>

              <FormControl 
                name="manhole"
                label="Горловина"
                onChange={this.handleChange('manhole')}
                value={this.manhole}
                placeholder="Введіть тип горловини"
                isLoading={this.isLoading}
                error={this.errors.has('manhole') ? this.errors.get('manhole') : ''}/>

              <FormControl 
                name="material"
                label="Матеріал виробу"
                onChange={this.handleChange('material')}
                value={this.material}
                placeholder="Введіть матеріал виробу"
                isLoading={this.isLoading}
                error={this.errors.has('material') ? this.errors.get('material') : ''}/>
            </div>

            <div className="column-right">
            
              <FormControl 
                name="volume"
                label="Об'єм"
                onChange={this.handleChange('volume')}
                value={this.volume}
                placeholder="Введіть об'єм"
                isLoading={this.isLoading}
                error={this.errors.has('volume') ? this.errors.get('volume') : ''}/>

              <FormControl 
                name="weight"
                label="Вага"
                onChange={this.handleChange('weight')}
                value={this.weight}
                placeholder="Введіть вагу"
                isLoading={this.isLoading}
                error={this.errors.has('weight') ? this.errors.get('weight') : ''}/>

              <div className="control-group">
                <label className="control-label" htmlFor="form-img-file">Мініатюра:</label>
                <div className="controls">            
                  <input type="file" id="form-img-file" onChange={this.handleImg('img')}/>
                  {this.renderImgPreview()}
                </div>          
              </div>

              <div className="control-group">
                <label className="control-label" htmlFor="form-img-thumb-file">Зображення:</label>
                <div className="controls">            
                  <input type="file" id="form-img-thumb-file" onChange={this.handleImg('imgThumb')}/>
                  {this.renderImgThumbPreview()}
                </div>          
              </div>

            </div>
          </div>

      </form>
    )
  }

  renderImgPreview() {
    if (!this.img.previewUrl) return null
    if (this.isImgRemoving) return (<div>Триває видалення мініатюри</div>)

    return (
      <ImagePreview 
        imageSrc={this.img.previewUrl}
        imageAlt="Попередній перегляд мініатюри"
        removeImageBtnClickHandler={this.handleRemoveImg}
        removeImageBtnWrapperClassName="help-inline"
        removeImageBtnIcon="zmdi zmdi-delete"
        removeImageBtnText="Видалити"/>
    )
  }  

  renderImgThumbPreview() {
    if (!this.imgThumb.previewUrl) return null
    if (this.isImgThumbRemoving) return (<div>Триває видалення зображення</div>)

    return (
      <ImagePreview 
        imageSrc={this.imgThumb.previewUrl}
        imageAlt="Попередній перегляд зображення" 
        removeImageBtnClickHandler={this.handleRemoveImgThumnb}
        removeImageBtnWrapperClassName="help-inline"
        removeImageBtnIcon="zmdi zmdi-delete"
        removeImageBtnText="Видалити"/>
    )
  }

  @action doRemoveImg = (isThumbs = false) => {
    const { store, api } = this.props

    if (isThumbs) this.isImgThumbRemoving = true
    else this.isImgRemoving = true

    api
      .product
      .removeImg(this.props.product.id, isThumbs)
      .then(action((result) => {
        store.products.save(result)
       
        if (isThumbs) {
          this.imgThumb.previewUrl = ''
          this.isImgThumbRemoving = false
        } else {
          this.img.previewUrl = ''
          this.isImgRemoving = false
        }
               
        this.flashMessages.success(`${isThumbs ? 'Мініатоюра' : 'Зображення'} успішно видалено`, 'Успіх.')
      }))
      .catch(action((error) => {        
        if (isThumbs) this.isImgThumbRemoving = false
        else this.isImgRemoving = false

        this.flashMessages.error(`Не вдалося видалити ${isThumbs ? 'Мініатоюра' : 'Зображення'}`, 'Помилка.')        
      }))
  }

  handleRemoveImg = () => this.doRemoveImg(false)
 
  handleRemoveImgThumnb = () => this.doRemoveImg(true)

  @action handleSubmit = (evt) => {
    evt.preventDefault()

    const { store, api, match } = this.props
    
    this.isLoading = true
    
    api.product
      .save({
        ...this.product,
        catalogId: match.params.catalogId
      })
      .then(action((result) => {
        this.isLoading = false        
        store.products.save(result)
        this.flashMessages.success('Каталог успішно додано', 'Успіх.')
        this.gotoViewProduct(result.id)
      }))
      .catch(action((error) => {        
        this.isLoading = false

        if (error.response) {
          if (typeof error.response.data.error === 'string') {
            this.flashMessages.error(error.response.data.error, 'Помилка.')
          } else {
            this.flashMessages.error('Деякі дані введені не вірно', 'Помилка.')
            this.errors.replace(
              api.product.normalizeErrors(error.response.data))
          }          
        } else {
          this.flashMessages.error("Х'юстон, у нас проблема", 'Помилка.')
        }
      }))
  }


  @action populate(product) {
    this.title = product.title
    this.palett = product.palett
    this.diameter = product.diameter
    this.height = product.height

    // optional
    this.price = product.price || ''
    this.priceUAH = product.priceUAH ? (product.priceUAH/100 + '') : 0
    this.color = product.color || ''
    this.manhole = product.manhole || ''
    this.material = product.material || ''
    this.volume = product.volume || ''
    this.weight = product.weight || ''

    this.img.previewUrl = product.imgSrc || ''    
    this.imgThumb.previewUrl = product.imgSrcBig || ''
  }

  handleImg = (img) => action((evt) => {
    const reader = new FileReader()
    const file = evt.target.files[0]

    reader.onloadend = action(() => {
      this[img].file = file
      this[img].previewUrl = reader.result
    })

    reader.readAsDataURL(file)
  })

  handleChange = (input) => action((evt) => {    
    this[input] = evt.target.value        
    if (this.errors.has(input)) this.errors.delete(input)
  })

  gotoViewProduct = (productId) => {
    const { match: { url }, history } = this.props
    const baseUrl = url.slice(0, url.lastIndexOf('/'))

    history.push(this.isProductEditing
      ? baseUrl : `${baseUrl}/product/${productId}`) 
  }

  static propTypes = {
    product: PropTypes.object
  }

  static defaultProps = {
    product: {}
  }
}

export default ProductForm