import React from 'react'
import { Switch, Route, Link, withRouter } from 'react-router-dom'
import { computed } from 'mobx'
import { inject } from 'mobx-react'
import breadcrumbs from 'app/components/common/breadcrumbs'
import RelatedProducts from './related'
import EditProduct from './edit'
import DeleteProduct from './delete'

@withRouter
@inject('store', 'config')
@breadcrumbs('Товар :title')
class ViewProduct extends React.PureComponent {

  constructor(props) {
    super(props)
    
    if (this.product) {
      props.mutateBreadcrumbsLastItem((bcItem) => ({
        ...bcItem,
        title: bcItem.title.replace(':title', this.product.title)
      }))
    }
  }
  
  componentWillMount() {
    if (!this.product) this.gotoIndex()
  }  

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.location.pathname !== nextProps.location.pathname
  }
  
  componentWillUpdate(nextProps) {
    nextProps.mutateBreadcrumbsLastItem((bcItem) => ({
      ...bcItem,
      title: bcItem.title.replace(':title', nextProps.store.products.byId(nextProps.match.params.productId).title)
    }))
  }
  
  @computed get product() {    
    const { store, match } = this.props    
    const { productId } = match.params

    if (!store.products.has(productId)) return null
  
    const product = store.products.byId(productId)

    return {
      ...product,
      priceUAH: product.priceUAH/100
    }
  }

  render() {
    if (!this.product) return null
    const { match } = this.props

    return (
      <Switch>
        <Route path={`${match.path}/edit`} component={EditProduct} />
        <Route path={`${match.path}/delete`} component={DeleteProduct} />
        <Route path={`${match.path}/related-products*`} component={RelatedProducts} />
        <Route render={this.renderView}/>
      </Switch>
    )
  }

  renderView = () => {
    const product = this.product

    return (
      <div className="view-product">

        <div className="content-header">
          {this.renderActions()}
        </div>

        <div className="row-fluid">

          <span className="span6">

            <ul className="pc-list">
              <li>
                <strong>Назва:</strong> {product.title}
              </li>
              <li>
                <strong>Ціна:</strong> {product.price}
              </li>
              <li>
                <strong>Ціна в грн.:</strong> {product.priceUAH}
              </li>
              <li>
                <strong>В упаковці:</strong> {product.palett}
              </li>
              <li>
                <strong>Колір:</strong> {product.color}
              </li>
              <li>
                <strong>Діаметер:</strong> {product.diameter}
              </li>
              <li>
                <strong>Висота:</strong> {product.height}
              </li>
              <li>
                <strong>Горловина:</strong> {product.manhole}
              </li>
              <li>
                <strong>Матеріал виробу:</strong> {product.materail}
              </li>
              <li>
                <strong>Об'єм:</strong> {product.volume}
              </li>
              <li>
                <strong>Вага:</strong> {product.weight}
              </li>              
            </ul>
          </span>

          <span className="span6">
            <ul className="pc-list">
              {product.imgSrc && 
                <li>
                  <strong>Мініатюра:</strong> <img src={product.imgSrc} alt="Мініатюра" className="img-polaroid" />
                </li>
              }

              {product.imgSrcBig &&   
                <li>
                  <strong>Зображення:</strong> <img src={product.imgSrcBig} alt="Зображення" className="img-polaroid" />
                </li>
              }
            </ul>
          </span>

        </div>
      </div>
    )
  }

  renderActions() {
    const { match } = this.props
    return (
      <ul className="nav nav-pills">           
        <li>
          <Link to={`${match.url}/edit`}>
            <i className="zmdi zmdi-edit"></i> Редагувати
          </Link>
        </li>
        <li><Link to={`${match.url}/delete`}>
          <i className="zmdi zmdi-delete"></i> Видалити
        </Link>
        </li>
        <li>
          <Link to={`${match.url}/related-products`}>
            <i className="zmdi zmdi-attachment"></i> Асоційовані товари
          </Link>
        </li>
      </ul>
    )
  }

  gotoIndex = () => {
    const { config, history  } = this.props
    const currentLanguage = config.languages.current()
    const url = `/${currentLanguage}${config.urlFor('catalog-index')}`
    history.replace(url)
  }

}

export default ViewProduct