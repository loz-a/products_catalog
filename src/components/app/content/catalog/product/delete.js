import React, { PropTypes } from 'react'
import { computed, observable, action } from 'mobx'
import { observer, inject } from 'mobx-react'
import { withRouter } from 'react-router-dom'
import breadcrumbs from 'app/components/common/breadcrumbs'
import Loader from 'app/components/common/loader'

@withRouter
@breadcrumbs('Видалити товар')
@inject('store', 'api', 'config')
@observer
class DeleteProduct extends React.PureComponent {

  @observable isLoading = false

  @computed get product() {
    const { store, match } = this.props
    return store.products.byId(match.params.productId)
  }

  @computed get flashMessages() {
    return this.props.store.flashMessages
  }

  render() {
    return (
      <Loader show={this.isLoading} message={'Перекуріть. Дані обробляються...'}>
        
        <div className="alert alert-block alert-error">        
          <h4 className="alert-heading">Увага!</h4>
          <p>
            Ви дійсно хочете видалити продукт {this.product.title}  
          </p>
          <p>
            <a className="btn btn-danger" onClick={this.handleOk}>
              <i className="zmdi zmdi-delete"></i> Видалити
            </a> <a className="btn" onClick={this.handleCancel}>
              <i className="zmdi zmdi-undo"></i> Не видаляти
            </a>
          </p>
        </div>

      </Loader>
    )
  }

  @action handleOk = (evt) => {
    evt.preventDefault()
    
    const { store, api } = this.props
    this.isLoading = true

    api.product
      .deleteById(this.product.id)
      .then(action((result) => {
        this.isLoading = false
        store.products.delete(this.product)
        this.flashMessages.success(`Товар успішно видалено`, 'Успіх.')
        this.gotoCatalog()
      }))
      .catch(action((error) => {        
        this.isLoading = false

        if (error.response) {
          const msg = typeof error.response.data.error === 'string' 
            ? error.response.data.error
            : 'Деякі дані введені не вірно'

          this.flashMessages.error(msg, 'Помилка.')
        } else {
          this.flashMessages.error("Х'юстон, у нас проблема", 'Помилка.')
        }
      }))
  }

  handleCancel = (evt) => {
    evt.preventDefault()
    const { history, match: { url: matchUrl } } = this.props    
    const url = matchUrl.slice(0, matchUrl.lastIndexOf('/'))
    history.replace(url)    
  }

  gotoCatalog = () => {
    const { config, history, match: { params } } = this.props
    const currentLanguage = config.languages.current()
    const url = `/${currentLanguage}${config.urlFor('catalog-index')}/${params.catalogId}`
    history.replace(url)
  }

  static propTypes = {
    store: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired
  }
}

export default DeleteProduct