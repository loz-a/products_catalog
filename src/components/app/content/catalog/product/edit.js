import React from 'react'
import { withRouter, Route } from 'react-router-dom'
import { inject, observer } from 'mobx-react'
import breadcrumbs from 'app/components/common/breadcrumbs'
import ProductForm from './form'

const EditProduct = ({
  location, 
  match, 
  store
}) => {

  const { productId } = match.params  
  const product = store.products.byId(productId)  

  return (
    <ProductForm product={product}/>
  )
}

export default withRouter(
  inject('store')(
    breadcrumbs('Редагувати товар')(
      observer(EditProduct)
    )
  )
)