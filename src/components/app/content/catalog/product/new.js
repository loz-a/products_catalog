import React from 'react'
import breadcrumbs from 'app/components/common/breadcrumbs'
import ProductForm from './form'

const NewProduct = () => (
  <ProductForm/>
)

export default breadcrumbs('Додати товар')(NewProduct)