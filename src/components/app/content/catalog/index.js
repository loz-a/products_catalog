import React from 'react'
import { Switch, Route } from 'react-router'
import { computed } from 'mobx'
import { inject } from 'mobx-react'
import breadcrumbs from 'app/components/common/breadcrumbs'
import ViewProduct from './product'
import EditCatalog from './edit'
import DeleteCatalog from './delete'
import CatalogActions from './actions'
import NewProduct from './product/new'
import './styles.css'

@inject('store', 'config')
@breadcrumbs('Каталог ":title"')
class ViewCatalog extends React.PureComponent {

  constructor(props) {
    super(props)
    props.mutateBreadcrumbsLastItem((bcItem) => {      
      return {
        ...bcItem,
        title: bcItem.title.replace(':title', this.catalog.title)
      }
    })
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.location.pathname !== nextProps.location.pathname
  }
  
  componentWillUpdate(nextProps) {
    nextProps.mutateBreadcrumbsLastItem((bcItem) => {      
      return {
        ...bcItem,
        title: bcItem.title.replace(':title', nextProps.store.catalogs.byId(nextProps.match.params.catalogId).title)
      }
    })
  }
  
  @computed get catalog() {    
    const { store, match } = this.props       
    return store.catalogs.byId(match.params.catalogId)
  }

  @computed get languages() {
    return this.props.config.languages
  }

  render() {
    const { match } = this.props

    return (
      <Switch>
        <Route path={`${match.path}/product/:productId(\\d+)`} component={ViewProduct}/>
        <Route path={`${match.path}/new-product`} component={NewProduct}/>
        <Route path={`${match.path}/edit`} component={EditCatalog}/>
        <Route path={`${match.path}/delete`} component={DeleteCatalog}/>        
        <Route render={this.renderView}/>
      </Switch>      
    )
  }

  renderView = () => {
    const catalog = this.catalog

    return (
      <div className="view-catalog">
        <div className="content-header">
          <CatalogActions/>
        </div>

        <ul className="pc-list">
          <li>
            <strong>Назва:</strong> {this.renderTitleTranslations()} 
          </li>
          <li>
            <strong>Псевдонім:</strong> {catalog.alias}
          </li>
          <li>
            {catalog.icon && <strong>Зображення:</strong>}
            {catalog.icon &&                  
                <img src={catalog.icon} alt="Зображення каталога" className="img-polaroid" />                  
            }            
          </li>
        </ul>
      </div>   
    )
  }  

  renderTitleTranslations = () => {
    const translations = this.catalog.translations
    const langs = this.languages
    const currLang = langs.byId(langs.current())
    
    return (
      <ul>
        <li title={currLang.locale.locale}>
          <i className={`${currLang.locale.locale.slice(-2).toLowerCase()} flag`}></i> {currLang.description} - {this.catalog.title}
        </li>

        { Object.values(langs.availables()).map((lang) => {          
            if (lang.language === currLang.language) return null            

            const country = lang.locale.locale.slice(-2).toLowerCase()            
            const title = translations.hasOwnProperty(lang.language) ? translations[lang.language].title : ''
            
            return (
              <li key={`${this.catalog.id}-${lang.id}`} title={lang.locale.locale}>
                <i className={`${country} flag`}></i> {lang.description} - {title}
              </li>
            )
            
          })
        }

      </ul>
    )
  }

}

export default ViewCatalog