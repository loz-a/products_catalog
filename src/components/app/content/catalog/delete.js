import React, { PropTypes } from 'react'
import { computed, observable, action } from 'mobx'
import { observer, inject } from 'mobx-react'
import { withRouter } from 'react-router-dom'
import breadcrumbs from 'app/components/common/breadcrumbs'
import Loader from 'app/components/common/loader'

@withRouter
@breadcrumbs('Видалити')
@inject('store', 'api', 'config')
@observer
class DeleteCatalog extends React.PureComponent {

  @observable isLoading = false

  @computed get catalog() {
    const { store, match } = this.props
    return store.catalogs.byId(match.params.catalogId)
  }

  @computed get flashMessages() {
    return this.props.store.flashMessages
  }

  render() {
    return (
      <Loader show={this.isLoading} message={'Перекуріть. Дані обробляються...'}>
        
        <div className="alert alert-block alert-error">        
          <h4 className="alert-heading">Увага!</h4>
          <p>
            Ви дійсно хочете видалити каталог {this.catalog.title}  
          </p>
          <p>
            <a className="btn btn-danger" onClick={this.handleOk}>
              <i className="zmdi zmdi-delete"></i> Видалити
            </a> <a className="btn" onClick={this.handleCancel}>
              <i className="zmdi zmdi-undo"></i> Не видаляти
            </a>
          </p>
        </div>

      </Loader>
    )
  }

  @action handleOk = (evt) => {
    evt.preventDefault()
    
    const { store, api } = this.props
    this.isLoading = true

    api.catalog
      .deleteById(this.catalog.id)
      .then(action((result) => {
        this.isLoading = false
        store.catalogs.delete(this.catalog)
        this.flashMessages.success(`Каталог успішно видалено`, 'Успіх.')
        this.gotoCatalogsIndex()
      }))
      .catch(action((error) => {
        this.isLoading = false
        
        if (typeof error.response.data.error === 'string') {
          this.flashMessages.error(error.response.data.error, 'Помилка.')
        } else {
          this.flashMessages.error("Х'юстон, у нас проблема", 'Помилка.')
        }
      }))
  }

  handleCancel = (evt) => {
    evt.preventDefault()
    const { history, match: { url: matchUrl } } = this.props    
    const url = matchUrl.slice(0, matchUrl.lastIndexOf('/'))
    history.replace(url)    
  }

  gotoCatalogsIndex = () => {
    const { config, history } = this.props
    const currentLanguage = config.languages.current()
    const url = `/${currentLanguage}${config.urlFor('catalog-index')}`
    history.replace(url)
  }

  static propTypes = {
    store: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired
  }
}

export default DeleteCatalog
