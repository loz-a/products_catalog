import React from 'react'
import CatalogForm from './form/base'
import breadcrumbs from 'app/components/common/breadcrumbs'
import LanguagesTabs from './form/langs-tabs'

const NewCatalog = () => (
  <LanguagesTabs onlyCurrentLanguageTabActive={true}>
    <CatalogForm/>
  </LanguagesTabs>
)


export default breadcrumbs('Новий')(NewCatalog)