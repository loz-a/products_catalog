import React, { PropTypes } from 'react'
import { withRouter } from 'react-router-dom'
import { computed, observable, action, reaction } from 'mobx'
import { observer, inject } from 'mobx-react'
import cx from 'classnames'
import ImagePreview from 'app/components/common/image-preview'
import Loader from 'app/components/common/loader'
import inputFilter from 'app/input-filter/catalog'
import { spaceToUnderscore } from 'app/utils/strings'

@withRouter
@inject('store', 'config', 'api')
@observer
class CatalogForm extends React.PureComponent {

  @observable title = ''
  @observable alias = ''
  @observable icon = observable.shallowObject({ file: null, previewUrl: ''})

  @observable errors = observable.map({})
  @observable isLoading = false
  @observable isAliasEditing = false
  @observable isCatalogIconRemoving = false
  
  constructor(props) {
    super(props)  

    if (this.isCatalogEditing) {
      this.populate(props.catalog)
    }
    this.handleAliasEditingMode()    
  }

  componentWillReceiveProps(nextProps) {
    this.populate(nextProps.catalog)
  }  

  componentWillUpdate(nextProps, nextState) {
    this.handleAliasEditingMode()    
  }

  @computed get selectedLanguage() {
    return this.props.selectedLanguage
  }

  @computed get flashMessages() {
    return this.props.store.flashMessages
  }

  @computed get isCatalogEditing() {
    return this.props.catalog.hasOwnProperty('id')
  }

  render() {        
    return (
       <Loader show={this.isLoading} message={'Перекуріть. Дані обробляються...'}>       
         {this.renderForm()}
       </Loader>
    )
  }

  renderForm = () => {      
    return (
      <form className="form-horizontal catalog-add" onSubmit={this.handleSubmit}>

        <div className="content-header">
          <ul className="nav nav-pills">                     
            <li>
              <button type="submit" className="btn btn-link" disabled={this.isLoading}>
                <i className="zmdi zmdi-check"></i> {this.isCatalogEditing ? 'Редагувати' : 'Додати'}
              </button>
            </li>
          </ul>
        </div>
   
        <div className={ cx('control-group', {'error': this.errors.has('title')}) }>
          <label className="control-label" htmlFor="form-title">Назва:</label>
          <div className="controls">
            <input type="text"
              id="form-title"               
              className="input-xlarge" 
              placeholder="Введіть назву каталога"
              onChange={this.handleChange('title')}              
              value={this.title}
              autoFocus={true}
              readOnly={this.isLoading}/>

              {this.errors.has('title') &&
                <span className="help-inline">
                  <i className="zmdi zmdi-alert-triangle"></i> {this.errors.get('title')}
                </span>
              }
          </div>
        </div>

        <div className={ cx('control-group', {'error': this.errors.has('alias')}) }>
          <label className="control-label" htmlFor="form-alias">Псевдонім:</label>
          <div className="controls">

            <div>
              <input type="text"
                id="form-alias" 
                className="input-xlarge" 
                placeholder="Введіть псевдо для каталога"
                value={this.alias}
                readOnly={(!this.isAliasEditing) || this.isLoading}
                onChange={this.handleChange('alias')}              
                onBlur={this.toggleAliasEditing}/>

                <span className="help-inline">
                  <a onClick={this.handleToggleAliasEditing} href="#"><i className="zmdi zmdi-edit"></i> ред.</a>
                </span>
              </div>


            {this.errors.has('alias') &&
              <span className="help-inline">
                <i className="zmdi zmdi-alert-triangle"></i> {this.errors.get('alias')}
              </span>
            }
          </div>
        </div>

        <div className="control-group">
          <label className="control-label" htmlFor="form-file">Зображення:</label>
          <div className="controls">            
            <input type="file" id="form-file" onChange={this.handleIcon}/>
            {this.renderPreview()}           
          </div>          
        </div>

      </form>  
    )
  }

  renderPreview = () => {
    if (!this.icon.previewUrl) return null
    if (this.isCatalogIconRemoving) return (<div>Триває видалення зображення</div>)
    
    return (
      <ImagePreview 
        imageSrc={this.icon.previewUrl}
        imageAlt="Попередній перегляд зображення" 
        removeImageBtnClickHandler={this.handleRemoveImage}
        removeImageBtnWrapperClassName="help-inline"
        removeImageBtnIcon="zmdi zmdi-delete"
        removeImageBtnText="Видалити"/>
    )
  }

  @action handleRemoveImage = (evt) => {
    const { store, api } = this.props
    this.isCatalogIconRemoving = true

    api
      .catalog
      .removeIcon(this.props.catalog.id)
      .then(action((result) => {
        store.catalogs.save(result)
        this.icon.previewUrl = null
        this.isCatalogIconRemoving = false
        this.flashMessages.success(`Зображення каталога успішно видалено`, 'Успіх.')
      }))
      .catch(action((error) => {
        this.isCatalogIconRemoving = false          
        this.flashMessages.error('Не вдалося видалити зображення', 'Помилка.')        
      }))
  }

  @action handleSubmit = (evt) => {
    evt.preventDefault()
    
    const { store, config, api } = this.props
    const { title, alias, icon: { file: icon } } = this
    this.isLoading = true

    const data = {title, alias, icon}
    if (this.isCatalogEditing) {
      data.id = this.props.catalog.id
    }

    inputFilter
      .asyncValidate(data, { store, config })
      .then(
        (validateResult) => {
          api.catalog
            .save(validateResult.data)
            .then(action((result) => {
              this.isLoading = false              
              store.catalogs.save(result)
              this.flashMessages.success('Каталог успішно додано', 'Успіх.')
              
              if (!this.isCatalogEditing) {
                this.gotoEditCatalog(result.id)
              }
            }))
            .catch(action((error) => {
              this.isLoading = false
              this.flashMessages.error('Деякі дані введені не вірно', 'Помилка.')
              this.errors.replace(error.response.data.error)
            }))
        },
        action((validateResult) => {
          this.isLoading = false
          this.errors.replace(validateResult.errors.get.everyFirstMessage())
          this.flashMessages.error('Деякі дані введені не вірно', 'Помилка.')
        })
      )
  }

  handleIcon = action((evt) => {
    const reader = new FileReader()
    const file = evt.target.files[0]

    reader.onloadend = action(() => {
      this.icon.file = file
      this.icon.previewUrl = reader.result
    })

    reader.readAsDataURL(file)
  })

  handleChange = (input) => action((evt) => {    
    this[input] = evt.target.value        
    if (this.errors.has(input)) this.errors.delete(input)
  })

  handleToggleAliasEditing = (evt) => {
    evt.preventDefault()
    this.toggleAliasEditing()
  }

  @action toggleAliasEditing = () => {
    this.isAliasEditing = !this.isAliasEditing
    if (!this.isAliasEditing) {
      this.alias = spaceToUnderscore((this.alias.length ? this.alias : this.title).trim())
    }
  }

  handleAliasEditingMode = () => {
    if (this.isAliasEditing && this.disposeTitleReaction) {
      this.disposeTitleReaction()
    } else if (!this.isAliasEditing && !this.alias) {
      this.disposeTitleReaction = reaction(
        () => this.title,
        () => this.alias = spaceToUnderscore(this.title)
      )
    }
  }

  @action populate(catalog) {
    this.title = catalog.title
    this.alias = catalog.alias

    if (catalog.hasOwnProperty('icon')) {
      this.icon.previewUrl = catalog.icon
    }
  }

  gotoEditCatalog = (catalogId) => {
    const { match: { url }, history } = this.props
    const baseUrl = url.slice(0, url.lastIndexOf('/'))
    history.push(`${baseUrl}/${catalogId}/edit`)
  }

  static propTypes = {
    catalog: PropTypes.object
  }

  static defaultProps = {
    catalog: {}
  }
}

export default CatalogForm