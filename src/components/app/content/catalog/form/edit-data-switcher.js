import React from 'react'
import { inject } from 'mobx-react'
import CatalogForm from './base'
import TranslateForm from './translate'

const EditDataSwitcher = ({ 
  config, 
  selectedLanguage, 
  catalog 
}) => {
  const params = { selectedLanguage, catalog }
  const isSelectedCurrentLang = config.languages.current() === selectedLanguage

  if (isSelectedCurrentLang) return (<CatalogForm {...params} />)  
  return (<TranslateForm selectedLanguage {...params} />)
}

export default inject('config')(EditDataSwitcher)
