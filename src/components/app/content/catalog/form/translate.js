import React, { PropTypes } from 'react'
import { computed, observable, action, autorun } from 'mobx'
import { inject, observer } from 'mobx-react'
import cx from 'classnames'
import Loader from 'app/components/common/loader'
import inputFilter from 'app/input-filter/translate-catalog'

@inject('store', 'api', 'config')
@observer
class TranslateForm extends React.PureComponent {

  @observable title
  @observable isLoading = false
  @observable errors = observable.map({})
    
  constructor(props) {      
    super(props)
    this.setTitleFromProps(props)
  }

  componentWillReceiveProps(nextProps) {    
    this.setTitleFromProps(nextProps)
  }

  @computed get selectedLang() {
    return this.props.selectedLanguage
  }

  @computed get flashMessages() {
    return this.props.store.flashMessages
  }

  @action setTitleFromProps(props) {
    const lang = props.selectedLanguage
    const tr = props.catalog.translations
    this.title = tr.hasOwnProperty(lang) ? tr[lang].title : ''
  }

  render() {
    return (
      <Loader show={this.isLoading} message={'Перекуріть. Дані обробляються...'}>       
        {this.renderForm()}
      </Loader>
    )
  }

  renderForm() {
    return (
      <form className="form-horizontal catalog-add" onSubmit={this.handleSubmit}>

        <div className="content-header">
          <ul className="nav nav-pills">                     
            <li>
              <button type="submit" className="btn btn-link" disabled={this.isLoading}>
                <i className="zmdi zmdi-check"></i> {'Редагувати'}
              </button>
            </li>
          </ul>
        </div>
   
        <div className={ cx('control-group', {'error': this.errors.has('title')}) }>
          <label className="control-label" htmlFor="form-title">Назва:</label>
          <div className="controls">
            <input type="text"
              id="form-title"               
              className="input-xlarge" 
              placeholder="Введіть назву каталога"
              onChange={this.handleTitleChange}              
              value={this.title}
              autoFocus={true}/>

              {this.errors.has('title') &&
                <span className="help-inline">
                  <i className="zmdi zmdi-alert-triangle"></i> {this.errors.get('title')}
                </span>
              }
          </div>
        </div>

      </form>   
    )
  }

  @action handleSubmit = (evt) => {
    evt.preventDefault()

    const { store, config, api } = this.props
    this.isLoading = true

    const data = { title: this.title }

    inputFilter
      .asyncValidate(data, { store, config })
      .then(
        (validateResult) => {
          const tr = this.props.catalog.translations
          const lang = this.selectedLang

          api.catalog
            .translate({
              title: validateResult.data.title,
              translation_id: tr.hasOwnProperty(lang) ? tr[lang].id : null,
              catalog_type_id: this.props.catalog.id,
              lang_id: config.languages.byId(lang).id
            })
            .then(action((result) => {
              this.isLoading = false              
              store.catalogs.save(result)
              this.flashMessages.success(`Каталог успішно додано`, 'Успіх.')
            }))
            .catch(action((error) => {
              this.isLoading = false
              this.flashMessages.error('Деякі дані введені не вірно', 'Помилка.')
              this.errors.replace(error.response.data.error)
            }))    
        },
        action((validateResult) => {
          this.isLoading = false
          this.errors.replace(validateResult.errors.get.everyFirstMessage())
          this.flashMessages.error('Дані введені не вірно', 'Помилка.')
        })
      )

  }

  @action handleTitleChange = (evt) => {    
    this.title = evt.target.value    
    if (this.errors.has('title')) this.errors.delete('title')
  }

  static propTypes = {
    selectedLanguage: PropTypes.string.isRequired,
    catalog: PropTypes.object.isRequired
  }
}

export default TranslateForm