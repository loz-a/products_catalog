import React, { PropTypes, cloneElement } from 'react'
import { observable, computed, action } from 'mobx'
import { withRouter } from 'react-router'
import { inject, observer } from 'mobx-react'
import Tabs, { DIRECTION_BOTTOM } from 'app/components/common/tabs'

@withRouter
@inject('config')
@observer
class LanguagesTabs extends React.PureComponent {

  @observable activeLanguage

  constructor(props) {
    super(props)
    
    this.setActiveLanguage(
      props.location.hash ? props.location.hash.slice(1) : props.config.languages.current()
    )
  }
  
  @action setActiveLanguage = (lang) => {
    this.activeLanguage = lang
  }

  handleTabClick = (evt) => {
    const hashLang = (evt.target.hash || evt.target.parentNode.hash)
    if (hashLang) {
      this.setActiveLanguage(hashLang.slice(1))
    }
  }

  render() {
    return (
      <Tabs direction={DIRECTION_BOTTOM}>
        {this.renderTabs()}        
        {cloneElement(this.props.children, {selectedLanguage: this.activeLanguage})}
      </Tabs>    
    )
  }

  renderTabs() {
    const { config, match, location, onlyCurrentLanguageTabActive } = this.props
    const paramLang = this.paramLanguage

    return Object
      .values(config.languages.availables())
      .map((lang) => {
        const isActive = this.activeLanguage === lang.language

        return (
        <Tabs.Tab key={lang.id} 
          href={isActive ? null : `${match.url}#${lang.language}`} 
          text={lang.language} 
          icon={`${lang.locale.locale.slice(-2).toLowerCase()} flag`}
          disabled={onlyCurrentLanguageTabActive ? (isActive ? false : true) : false}
          active={isActive}
          onClick={isActive ? null : this.handleTabClick}/>
        )
      })
  }

  static propTypes = {
    config: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    onlyCurrentLanguageTabActive: PropTypes.bool
  }

  static defaultProps = {
    onlyCurrentLanguageTabActive: false
  }
  
}

export default LanguagesTabs