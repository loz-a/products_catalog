import React from 'react'
import { withRouter, Route } from 'react-router-dom'
import { inject, observer } from 'mobx-react'
import EditDataSwitcher from './form/edit-data-switcher'
import breadcrumbs from 'app/components/common/breadcrumbs'
import LanguagesTabs from './form/langs-tabs'

const EditCatalog = ({ 
  location, 
  match, 
  store 
}) => {

  const { catalogId } = match.params
  const catalog = store.catalogs.byId(catalogId)

  return (
    <LanguagesTabs>      
      <EditDataSwitcher catalog={catalog}/>
    </LanguagesTabs>
  )
}

export default withRouter(
  inject('store')(
    breadcrumbs('Редагувати')(
      observer(EditCatalog)
    )
  )
)