import React from 'react'
import Tabs from '../../common/tabs'

const Wellcome = () => (
  <Tabs>
    <Tabs.Tab text="Головна"/>

    <div className="wellcome text-center">
      Каталог товарів до Ваших послуг
    </div>        
  </Tabs>
)


export default Wellcome