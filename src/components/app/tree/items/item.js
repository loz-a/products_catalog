import React, { PropTypes } from 'react'
import { findDOMNode } from 'react-dom'
import { NavLink, withRouter } from 'react-router-dom'
import { computed } from 'mobx'
import { observer, inject } from 'mobx-react'
import { DragSource, DropTarget } from 'react-dnd'
import pathToRegexp from 'path-to-regexp'
import cx from 'classnames'
import dndItemTypes from 'app/components/app/dnd/item-types'
import { CatalogPropType } from '../../prop-types'

const sourceSpec = {
  beginDrag(props) {    
    return {
      id: props.item.node.id,
      title: props.item.node.title
    }
  },
  
  endDrag(props, monitor) {
    const item = monitor.getItem()
    const dropResult = monitor.getDropResult()
  }
}

const targetSpec = {
  drop(props, monitor, component) {
    // console.log('src: monitor.getItem', monitor.getItem().id, monitor.getItem().title)
    // console.log('target: props.item', props.item.id, props.item.title)

    const { api, store, item, setIsLoading } = props
    const srcItemId = monitor.getItem().id
    const targetItemId = props.item.id
    
    setIsLoading(true)

    api.product
      .reorder(srcItemId, targetItemId)
      .then((data) => {
        store.products.populate(data)
        setIsLoading(false)
      })
  }
}

@inject('store', 'api')
@DropTarget(
  dndItemTypes.CATALOG_ITEM,
  targetSpec,
  (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop()
  })
)
@DragSource(
  dndItemTypes.CATALOG_ITEM, 
  sourceSpec, 
  (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  })
)
@withRouter
@observer
class Item extends React.PureComponent {

  static propTypes = {
    connectDragSource: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired,
    item: PropTypes.object.isRequired,
    parent: PropTypes.object.isRequired,
    api: PropTypes.object.isRequired,
    store: PropTypes.object.isRequired
  }

  @computed get item() {
    return this.props.item
  }

  @computed get parent() {
    return this.props.parent
  }

  @computed get productUrl() {  
    const { match } = this.props
    const baseUrl = pathToRegexp.compile(match.path)({catalogId: this.parent.id})   
    return `${baseUrl}/product/${this.item.id}`
  }

  render() {
    const item = this.item
    const { match, isDragging, connectDragSource, connectDropTarget, isOver, canDrop } = this.props
    const opacity = isDragging ? 0.4 : 1

    const itemCssClass = cx('tree-item', {'item-over': isOver})
    
    return connectDragSource(
      connectDropTarget(
        <li className={itemCssClass} style={{ opacity }}>
          <i className="zmdi zmdi-file-text"></i> 
          <NavLink 
            to={this.productUrl}
            title={item.title}>
            {item.title}
          </NavLink>
        </li>
      ),
      { dropEffect: 'copy' }
    )    
  }
}

export default Item