import React, { PropTypes } from 'react'
import { withRouter } from 'react-router-dom'
import { observable, computed, action } from 'mobx'
import { inject, observer } from 'mobx-react'
import pathToRegexp from 'path-to-regexp'
import { CatalogPropType } from '../../prop-types'
import Container from '../container'

@withRouter
@inject('api', 'store')
@observer
class ItemsContainer extends React.PureComponent {

  loaded = false
  @observable loading = false  
  
  get paramCatalogId() {   
    const result = pathToRegexp(this.props.match.path, []).exec(location.pathname)
    return typeof result[1] !== 'undefined' ? result[1] : null
  }  

  @computed get container() {
    return this.props.container
  }

  renderChildren = (children) => (<Container items={children} />)

  renderLoading = () => (<p>Loading...</p>)

  render() {
    const container = this.container
    
    return (
      <li className="tree-item-container">	        
        <input type="checkbox" 
          id={`item-${container.id}`} 
          onClick={this.onToggleOpen} 
          ref={ (input) => this.checkboxInput = input }/>

        <span className="zmdi brunch-container-icon icon-caret"></span>
        <span className="zmdi brunch-container-icon icon-folder"></span>

        <label htmlFor={`item-${container.id}`} onClick={this.viewCatalog} title={container.title}>
          {container.title}
        </label>

        {container.children &&
          this.renderChildren(container.children)
        }

        {this.loading && 
          this.renderLoading()
        }
      </li>
    )
  }

  onToggleOpen = () => {   
    if (!this.loaded) this.loadChildren()
  }

  viewCatalog = (evt) => {
    const opened = this.checkboxInput.checked
    const isActive = this.paramCatalogId == this.container.id

    if (opened && !isActive) {
      evt.preventDefault()
      this.checkboxInput.checked = true      
    }

    this.gotoCatalog()
  }

  @action loadChildren = () => { 

    // Promise.all([
    //   this.props.api.catalog.byParent(this.container),
    //   this.props.api.product.byCatalog(this.container)
    // ])
    // .then(
    //   (result) => {
    //     console.log(result)
    //   },
    //   (error) => { console.log(error) }
    // )

    this.loading = true
    this.props.api.product.byCatalog(this.container)
      .then(action((result) => {
        this.loading = false
        this.loaded = true
        this.props.store.products.populate(result)
      }))
  }

  gotoCatalog = () => {
    const { history, match } = this.props
    const path = pathToRegexp.compile(match.path)({catalogId: this.container.id})    
    history.push(path)
  }

  static propTypes = {
    // container: CatalogPropType.isRequired,
    api: PropTypes.object.isRequired,
    store: PropTypes.object.isRequired
  }
}

export default ItemsContainer