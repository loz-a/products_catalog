import React, { PropTypes } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { computed, observable, toJS } from 'mobx'
import { observer, inject } from 'mobx-react'
import transformNodes from './store/transformers'
import Container from './container'
import './styles.css'

@withRouter
@inject('store')
@observer
class Tree extends React.PureComponent {

  @observable filter = null

  @observable displayRoot = null

  @computed get catalogs() {    
    return this.props.store.catalogs.asTree()
  }

  @computed get displayCatalogs() {      
    return transformNodes(this)
  }

  render() {   
    const { match } = this.props

    return (
      <div className="treeview-container">
        <ul className="nav nav-pills nav-stacked css-treeview-action">
          <li>
            <Link to={`${match.url}/add-catalog`}>
              <i className="zmdi zmdi-playlist-plus zmdi-hc-lg"></i> Додати Каталог
            </Link>
          </li>
        </ul>

        <div className="css-treeview">
          <Container items={this.displayCatalogs}/>
        </div>
      </div>
    )
  }

  static propTypes = {
    store: PropTypes.object.isRequired
  }
}

export default Tree