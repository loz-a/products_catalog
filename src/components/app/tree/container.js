import React, { PropTypes } from 'react'
import { observable, action } from 'mobx'
import { observer } from 'mobx-react'
import { CatalogPropType } from '../prop-types'
import Item from './items/item'
import ItemsContainer from './items/container'

@observer
class Container extends React.PureComponent {

  @observable isLoading = false

  renderItems() {        
    return this.props.items.map(
      (item) => {        
        if (!item.isVisible) return null        
        return typeof item.children !== 'undefined'
          ? <ItemsContainer key={`c-${item.id}`} container={item}/> 
          : <Item key={`i-${item.id}`} item={item} parent={item.node.parent} setIsLoading={this.setIsLoading} />
      }
    )
  }


  render() {
    
    return (
      <ul>
        {this.renderItems()}
      </ul>      
    )
  }


  @action setIsLoading = (isLoading = true) => this.isLoading = isLoading

  static propTypes = {
    // items: PropTypes.arrayOf(CatalogPropType).isRequired
  }
}

export default Container
