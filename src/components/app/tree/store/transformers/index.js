import { extendObservable, createTransformer, action } from 'mobx'
import DisplayNodeContainer from '../display-node/container'
import DisplayNodeItem from '../display-node/item'

export default function(tree) {

  function displayNodeFactory(node, tree) {
    const isContainer = typeof node.children !== 'undefined'
    const args = [node, tree, transformNode]
    return isContainer ? new DisplayNodeContainer(...args) : new DisplayNodeItem(...args)
  }

  const transformNode = createTransformer((node) => displayNodeFactory(node, tree))

  return Object.values(tree.catalogs).map((item) => transformNode(item))
}

  