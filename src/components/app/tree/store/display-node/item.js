import { observable, computed } from 'mobx'

class DisplayNodeItem {
   
  constructor(node, tree, transformNode) {    
    this.node = node
    this.tree = tree
    this.transformNode = transformNode
  }

  @computed get id() { return  this.node.id }
  @computed get title() { return this.node.title }
  @computed get parentId() { return this.node.parent.id }

  @computed get isVisible() {
    return !this.tree.filter 
      || this.title.toLowerCase().indexOf(this.tree.filter.toLowerCase()) !== -1 
      || this.children.some((child) => child.isVisible)
  }

  @computed get path() {
    return this.node.parent === null ? this.title : this.transformNode(this.node.parent, this.tree).path + '/' + this.title
  }
}

export default DisplayNodeItem