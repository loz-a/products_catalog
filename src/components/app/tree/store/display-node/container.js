import { observable, computed, action } from 'mobx'
import DisplayNodeItem from './item'

class DisplayNodeContainer extends DisplayNodeItem {

  @computed get children() {
    return Object.values(this.node.children)
      .map((item) => this.transformNode(item, this.tree))
      .filter((child) => child.isVisible)
  }

}

export default DisplayNodeContainer