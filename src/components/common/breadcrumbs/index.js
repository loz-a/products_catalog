import React from 'react'
import { withRouter } from 'react-router-dom'
import { inject, observer } from 'mobx-react'
import Breadcrumbs from './component'


export default (title = null) => {

  return (CustomComponent) => {

    class DecoratedComponent extends React.PureComponent {
     
      componentWillMount() {
        const { match, history } = this.props
        if (title) this.props.breadcrumbs.add(title, match.url, history.location.pathname)
      }
      
      componentWillUpdate(nextProps, nextState) {
        if (this.props.location.pathname !== nextProps.location.pathname) {        
          const { match, history } = nextProps
          if (title) this.props.breadcrumbs.add(title, match.url, history.location.pathname)
        }
      }
            
      renderBreadcrumbs = (homeIcon) => {        
        return <Breadcrumbs homeIcon={homeIcon} items={this.props.breadcrumbs.items} />
      }

      mutateBreadcrumbsLastItem = (bcItem) => (mutator) => {
        const { breadcrumbs } = this.props
        const newBCItem = mutator(bcItem)
          
        if (!breadcrumbs.equalsItemValues(bcItem, newBCItem)) {          
          if (title) breadcrumbs.replaceLast(newBCItem)
          else breadcrumbs.add(newBCItem.title, newBCItem.path)
        }
      }

      render() {        
        return <CustomComponent {...this.props} 
          renderBreadcrumbs={this.renderBreadcrumbs}
          mutateBreadcrumbsLastItem = {this.mutateBreadcrumbsLastItem(this.props.breadcrumbs.last())}
          />        
      }
    }

    return inject('breadcrumbs')(withRouter(DecoratedComponent))
  }
}