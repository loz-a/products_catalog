import React from 'react'
import { extendObservable, observable, action, toJS, autorun } from 'mobx'
import { Provider } from 'mobx-react'

const breadcrumbsStore = new (function() {
  extendObservable(this, {
    items: observable.array([])
  })

  this.add = action((title, path, location) => {
    this.items.push({title, path, location})
    this.items.replace(this.items.filter((item) => item.location === location))
  })

  this.clear = action(() => this.items.clear())

  this.last = () => this.items.length ? toJS(this.items[this.items.length - 1]) : null

  this.replaceLast = action((replacement) => {
    this.items.pop()
    this.items.push(replacement)    
  }),

  this.equalsItemValues = (value1, value2) => Object.is(toJS(value1), toJS(value2))
})


export default ({ children }) => {  
  return (
  <Provider breadcrumbs={breadcrumbsStore}>
    {children}
  </Provider>
)}

