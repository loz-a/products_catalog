import React, { PropTypes } from 'react'
import { computed } from 'mobx'
import { observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import shortid from 'shortid'

const Divider = () => (
  <span className="divider">/</span>
)

@observer
class Breadcrumbs extends React.PureComponent {  

  @computed get items() {
    return this.props.items.map((item) => ({ ...item, id: shortid.generate() }))
  }

  renderListItems() {
    const listItems = []
    const items = this.items
    const { homeIcon } = this.props
    
    if (items.length === 1) {
      listItems.push(<li key={items[0].id}><i className={homeIcon}></i> {items[0].title} </li>)
    }

    if (items.length > 1) {
      const homeItem = items.shift()
      const home = (
        <li key={homeItem.id}>
          <i className={homeIcon}></i> <Link to={homeItem.path}>{homeItem.title}</Link>
          <Divider />
        </li>
      )

      const activeItem = items.pop()
      const active = (<li key={activeItem.id} className="active">{activeItem.title}</li>)

      listItems.push(
        home,
        ...items.map((item) => (
          <li key={item.id}>
            <Link to={item.path}>{item.title}</Link> <Divider/>
          </li>
        )),
        active
      )
    }
    return listItems
  }

  render() {
    return (
      <ul className="breadcrumb">
       { this.renderListItems() }
     </ul>
    )
  }
}


Breadcrumbs.propTypes = {
  // items: PropTypes.arrayOf(
  //   PropTypes.shape({
  //     title: PropTypes.string.isRequired,
  //     path: PropTypes.string
  //   })
  // ),
  homeIcon: PropTypes.string
}

Breadcrumbs.defaultProps = {
  homeIcon: 'zmdi zmdi-gps-dot'
}

export default Breadcrumbs
