import React, { PropTypes } from 'react'
import { Link } from 'react-router'

const Edit = ({
  href,
  value,
  onClick,
  ...props
}) => (
  <Link onClick={onClick} to={href} {...props}>
    <i className="zmdi zmdi-edit"></i> {value}
  </Link>
)

Edit.propTypes = {
  href: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onClick: PropTypes.func
}

export default Edit
