import React, { PropTypes } from 'react'
import { Link } from 'react-router'

const Cancel = ({
  href,
  value,
  onClick,
  ...props
}) => (
  <Link onClick={onClick} to={href} {...props}>
    <i className="zmdi zmdi-undo"></i> {value}
  </Link>
)

Cancel.propTypes = {
  href: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onClick: PropTypes.func
}

export default Cancel
