import React, { PropTypes } from 'react'
import './styles.css'

const ImagePreview = ({
  imageSrc,
  imageClassName = 'img-preview',
  imageAlt = 'Image Preview',
  removeImageBtnWrapperClassName = 'remove-image-btn-box',
  removeImageBtnClickHandler = null,
  removeImageBtnHref = '#',
  removeImageBtnIcon = null,
  removeImageBtnText = 'Delete'
}) => {
  return (
    <div className="img-preview-box">
      <img 
          src={imageSrc} 
          className={imageClassName}
          alt={imageAlt}/>
      
      {removeImageBtnClickHandler && 
        <span className={removeImageBtnWrapperClassName}>
          <a onClick={removeImageBtnClickHandler} href={removeImageBtnHref}>
            <i className={removeImageBtnIcon}></i> {removeImageBtnText}
          </a>
        </span>      
      }
    </div>
  )
}

ImagePreview.propTypes = {
  imageSrc: PropTypes.string.isRequired,
  imageClassName: PropTypes.string,
  imageAlt: PropTypes.string,
  removeImageBtnWrapperClassName: PropTypes.string,
  removeImageBtnClickHandler: PropTypes.func,
  removeImageBtnHref: PropTypes.string,
  removeImageBtnIcon: PropTypes.string,
  removeImageBtnText: PropTypes.string
}

export default ImagePreview