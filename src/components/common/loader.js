import React from 'react'
import Loader from 'react-loader-advanced'

const spnnrStyle = {
  background: '#fff',
  border: '1px solid #ddd',
  fontWeight: 'bold',
  padding: '10px 20px',
  height: '30px',
  lineHeight: '30px',
  display: 'inline-block'
}

const bcgndStyle = {
  backgroundColor: 'rgba(255,255,255,0.15)'
}

const frgndStyle = {
  color: "#333"
}

const spinner = (message, spinnerIcon, spinnerStyle) => {
  if (typeof message !== 'string') return message
  return (
    <span style={{...spnnrStyle, ...spinnerStyle}}>
      {spinnerIcon && <i className={spinnerIcon}/>} {message}
    </span>
  )
}

export default ({ 
  message, 
  spinnerIcon = null, 
  foregroundStyle = {},
  backgroundStyle = {},
  spinnerStyle = {},
  contentBlur=1,
  children, 
  ...otherProps 
}) => {   

  return (
    <Loader {...otherProps}       
      foregroundStyle={{...frgndStyle, ...foregroundStyle}}
      backgroundStyle={{...bcgndStyle, ...backgroundStyle}}
      contentBlur={contentBlur}
      message={spinner(message, spinnerIcon, spinnerStyle)}
    >
      {React.Children.only(children)}
    </Loader>
  )
}
