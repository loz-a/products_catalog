import React, { PropTypes, cloneElement, Children } from 'react'
import Tab from './tab'

const filterChildren = (children, condition) => {
  if (!children) return []
  return Children.map(children, (child, idx) => {
    if (child && child.type && condition(child)) return cloneElement(child, {key: idx})
  })
}

const DIRECTION_TOP = 'top'
const DIRECTION_LEFT = 'left'
const DIRECTION_RIGHT = 'right'
const DIRECTION_BOTTOM = 'bottom'

const Tabs = ({
  direction= DIRECTION_TOP,
  children
}) => {
  const tabs = filterChildren(children, (child) => child.type === Tab)
  const content = filterChildren(children, (child) => child.type !== Tab)

  if (direction === DIRECTION_BOTTOM) {
    return (
      <div className="tabbable tabs-below">
        <div className="tab-content">
          { content.length > 1 ? <div>{content}</div> : content }
        </div>

        <ul className="nav nav-tabs">
          {tabs}
        </ul>
      </div>
    )
  }
  const tabbableCssClass = (direction === DIRECTION_TOP) ? 'tabbable' : `tabbable tabs-${direction}`
  return (
    <div className={tabbableCssClass}>
      <ul className="nav nav-tabs">
        {tabs}
      </ul>

      <div className="tab-content">
        { content.length > 1 ? <div>{content}</div> : content }
      </div>
    </div>
  )
}

Tabs.propTypes = {
  children: PropTypes.node
}

Tabs.Tab = Tab

export { DIRECTION_TOP, DIRECTION_LEFT, DIRECTION_RIGHT, DIRECTION_BOTTOM }

export default Tabs
