import React, { PropTypes } from 'react'
import { Link } from 'react-router-dom'
import cx from 'classnames'

const clickHandler = (evt) => evt.preventDefault()

const Tab = ({
  text,
  href = null,
  icon = null,
  disabled = false,
  active = false,
  onClick = null
}) => {

  if (!href || active || disabled) return (
    <li className={cx({disabled, active})}>
      <a data-toggle="tab" onClick={onClick || clickHandler}>
        { icon && <i className={icon}></i> }
        {text}
      </a>
    </li>
  )

  return (
    <li>
      <Link to={href} data-toggle="tab" onClick={onClick}>
        { icon && <i className={icon}></i> }
        {text}
      </Link>
    </li>
  )
}

Tab.propTypes = {
  text: PropTypes.string.isRequired,
  href: PropTypes.string,
  icon: PropTypes.string,
  disabled: PropTypes.bool,
  active: PropTypes.bool,
  onClick: PropTypes.func
}

export default Tab
