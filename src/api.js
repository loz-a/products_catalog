import axios from 'axios'

const populateFormData = (data) => {
  const fd = new FormData()
  for (let prop in data) if (data.hasOwnProperty(prop)) {
    fd.append(prop, data[prop])
  }
  return fd
}

export default function (config) {

  const { urls, languages } = config
  const xhr = axios.create()

  xhr.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'

  this.catalog = {

    translate(data) {      
      const currentLanguage = config.languages.current()
      const formData = populateFormData(data)

      const url = `/${currentLanguage}${config.urlFor('catalog-translate')}`        

      return xhr
        .post(url, formData)
        .then((result) => result.data)
    },

    removeIcon: (id) => {
      const currentLanguage = config.languages.current()
      const formData = new FormData()

      formData.append('id', id)

      return xhr
        .post(`/${currentLanguage}${config.urlFor('catalog-remove-icon')}`, formData)
        .then(
          (result) => result.data
        )
    },

    save: (data) => {
      const formData = new FormData()
      if (data.id) formData.append('id', data.id)
      formData.append('title', data.title)
      formData.append('alias', data.alias)
      formData.append('icon_src', data.icon)

      const currentLanguage = config.languages.current()
      let url = ''
      if (data.id) {
        url = `/${currentLanguage}${config.urlFor('catalog-edit')}/${data.id}`
      } else {
        url = `/${currentLanguage}${config.urlFor('catalog-add')}`
      }
            
      return xhr
        .post(url, formData)
        .then(
          (result) => result.data 
        )
    },

    deleteById: (id) => {
      const currentLanguage = config.languages.current()
      const formData = new FormData()

      formData.append('id', id)

      return xhr
        .post(`/${currentLanguage}${config.urlFor('catalog-delete')}`, formData)
        .then(
          (result) => result.data
        )
    }
  }

  this.product = {

    byCatalog: (catalog) => {
      return xhr
        .get(`/${config.languages.current()}${config.urlFor('product-load')}?catalog_type=${catalog.id}`)
        .then((result) => result.data)
    },

    save: (data) => {
      const formData = new FormData()
      if (data.id) formData.append('id', data.id)

      formData.append('catalog_type', data.catalogId)
      formData.append('name', data.title)
      formData.append('height', data.height)
      formData.append('volume', data.volume)
      formData.append('weight', data.weight)
      formData.append('color', data.color)      
      formData.append('diameter', data.diameter)
      formData.append('manhole', data.manhole)
      formData.append('material', data.material)
      formData.append('palett', data.palett)
      formData.append('palett_height', data.price)
      formData.append('price_uah', data.priceUAH)
      formData.append('img_src', data.imgSrc)
      formData.append('img_src_big', data.imgSrcBig)

      const currentLanguage = config.languages.current()
      let url = ''
      if (data.id) {
        url = `/${currentLanguage}${config.urlFor('product-edit')}/${data.id}`
      } else {
        url = `/${currentLanguage}${config.urlFor('product-add')}`
      }
            
      return xhr
        .post(url, formData)
        .then((result) => result.data)
    },

    deleteById: (id) => {
      const currentLanguage = config.languages.current()
      const formData = new FormData()

      formData.append('id', id)

      return xhr
        .post(`/${currentLanguage}${config.urlFor('product-delete')}`, formData)
        .then(
          (result) => result.data
        )
    },

    reorder: (reorderedItemId, prevReorderedItemId) => {
      const currentLanguage = config.languages.current()
      const url = `/${currentLanguage}${config.urlFor('product-reorder')}`

      return xhr
        .post(url, populateFormData({
          reorder_id: reorderedItemId,
          reorder_id_before: prevReorderedItemId
        }))
        .then(
          (result) => result.data
        )

    },

    removeImg: (id, thumb = false) => {
      const currentLanguage = config.languages.current()
      const formData = new FormData()

      formData.append('id', id)

      const url = thumb ? 'product-remove-img-big': 'product-remove-img'
      return xhr
        .post(`/${currentLanguage}${config.urlFor(url)}`, formData)
        .then(
          (result) => result.data
        )
    },

    normalizeErrors: (data) => {
      const errors = {}
      if (data.error.name) errors.title = data.error.name 
      if (data.error.height) errors.height = data.error.height
      if (data.error.volume) errors.volume = data.error.volume
      if (data.error.weight) errors.weight = data.error.weight
      if (data.error.color) errors.color = data.error.color      
      if (data.error.diameter) errors.diameter = data.error.diameter
      if (data.error.manhole) errors.manhole = data.error.manhole
      if (data.error.material) errors.material = data.error.material
      if (data.error.palett) errors.palett = data.error.palett
      if (data.error.palett_height) errors.price = data.error.palett_height
      if (data.error.price_uah) errors.priceUAH = data.error.price_uah
      if (data.error.img_src) errors.imgThumb = data.error.img_src
      if (data.error.img_src_big) errors.img = data.error.img_src_big  
      return errors
    },

    related: {
      byOwnerId: (owner_id) => {
        const currentLanguage = config.languages.current()
        const url = `/${currentLanguage}${config.urlFor('related-products-load')}`

        return xhr
          .get(url, {params: {owner_id}})
          .then((result) => result.data)
      },

      save: (data) => {
        const currentLanguage = config.languages.current()
        const url = `/${currentLanguage}${config.urlFor('related-products-save')}`
        
        const { ownerId, data: relatedData } = data

        return xhr
          .post(url, {
            owner_id: ownerId,
            data: relatedData
          })
          .then((result) => result.data)
      }
    }
  }
}