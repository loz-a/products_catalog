export const isObject = (val) => {
  return val != null && typeof val === 'object' && Array.isArray(val) === false;
}

export const hasCallableProperty = (obj, property) => {
  if (isObject(obj)) {
    return obj.hasOwnProperty(property) && typeof obj[property] === 'function'
  }
  return false
}

export const hasPropertyInstanceOf = (obj, property, Instance) => {
  if (isObject(obj)) {
    return obj.hasOwnProperty(property) && obj[property] instanceof Instance
  }
  return false
}