import { PropTypes } from 'react'
import { PropTypes as ObservablePropTypes } from 'mobx-react'

export const CatalogPropType = PropTypes.shape({
    id: PropTypes.string.isRequired,
    alias: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    icon: PropTypes.string,
    children: PropTypes.oneOfType([
      PropTypes.bool,
      ObservablePropTypes.observableArrayOf(PropTypes.object)
    ]).isRequired
  })