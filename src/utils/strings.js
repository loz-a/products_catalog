export const toCamelCase = (str) => {  
    return str.replace(/((\_|\-)\w)/g, (match) => match[1].toUpperCase())
}

export const toUnderscore = (str) => {
  return str.replace(/([A-Z])/g, (match) => "_" + match.toLowerCase())
}

export const spaceToUnderscore = (str) => {
  return str.replace(/\s/g, '-').toLowerCase()
}